package com.example.tydes.realtimestressmonitor;


import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.empatica.empalink.ConnectionNotAllowedException;
import com.empatica.empalink.EmpaDeviceManager;
import com.empatica.empalink.config.EmpaSensorStatus;
import com.empatica.empalink.config.EmpaSensorType;
import com.empatica.empalink.config.EmpaStatus;
import com.empatica.empalink.delegate.EmpaDataDelegate;
import com.empatica.empalink.delegate.EmpaStatusDelegate;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity implements EmpaDataDelegate, EmpaStatusDelegate {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private static final long STREAMING_TIME = 200000; // Stops streaming 10 seconds after connection
    private static final String EMPATICA_API_KEY = "881832867e544566a814ba419a950ed1"; // TODO insert your API Key here
    private static final Random RANDOM = new Random();
    public static final String TAG = "MainActivity";
    private float ibiArraySize;
    private float fCurTimeIBI;
    private float ibiSum;
    private float edaSum;
    private float tempSum;
    private float dIBI;
    private float dBVP;
    private float ibiStdSum;
    private float ibiStd;
    private float ibiAverage;
    private float edaAverage;
    private float tempAverage;
    private float heartRate;
    private float nn50Count;
    private float pnn50;
    private float fCurrTimeBvp;
    private float dInitialTime;
    private int lastX = 0;
    private int graphHieght;
    private int ibiSampleTimeDifference;
    private LineGraphSeries<DataPoint> bvpSeries;
    private GraphView graph;
    private TextView statusLabel;
    private TextView deviceNameLabel;
    private TextView heartRateLabel;
    private TextView edaRateLabel;
    private TextView tempRateLabel;
    private EmpaDeviceManager deviceManager = null;
    private DescriptiveStatistics stats = new DescriptiveStatistics();
    private List<Float> ibiSample;
    private List<Float> ibiSampleTimeStamps;
    private List<Float> bvpSample;
    private List<Float> edaSample;
    private List<Float> tempSample;
    private List<Float> bvpampleTimeStamps;
    private List<Float> hrSample;
    private String currentTime;
    private Long initialTime;
    private Long lCurTimeIBI;
    private Long lCurTimeBVP;
    private Boolean firstTimeStampRead;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusLabel = (TextView) findViewById(R.id.status);
        deviceNameLabel = (TextView) findViewById(R.id.deviceName);
        heartRateLabel = (TextView) findViewById(R.id.heartRateLabel);
        edaRateLabel = (TextView) findViewById(R.id.edaLabel);
        tempRateLabel = (TextView) findViewById(R.id.tempLabel);

        firstTimeStampRead = true;
        ibiSample = new ArrayList<Float>();
        ibiSampleTimeStamps = new ArrayList<Float>();
        bvpSample = new ArrayList<Float>();
        edaSample = new ArrayList<Float>();
        tempSample = new ArrayList<Float>();
        bvpampleTimeStamps = new ArrayList<Float>();
        bvpSeries = new LineGraphSeries<DataPoint>();
        graph = (GraphView) findViewById(R.id.statsGraph);

        graph.addSeries(bvpSeries);
        Viewport viewport = graph.getViewport();
        viewport.setScalable(true);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(8);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.setTitle("BVP");
        graph.setTitleTextSize(80);
        graph.setTitleColor(Color.WHITE);

        initEmpaticaDeviceManager();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult Method");
        switch (requestCode) {
            case REQUEST_PERMISSION_ACCESS_COARSE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted, yay!
                    initEmpaticaDeviceManager();
                } else {
                    // Permission denied, boo!
                    final boolean needRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION);
                    new AlertDialog.Builder(this)
                            .setTitle("Permission required")
                            .setMessage("Without this permission bluetooth low energy devices cannot be found, allow it in order to connect to the device.")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // try again
                                    if (needRationale) {
                                        // the "never ask again" flash is not set, try again with permission request
                                        initEmpaticaDeviceManager();
                                    } else {
                                        // the "never ask again" flag is set so the permission requests is disabled, try open app settings to enable the permission
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }
                                }
                            })
                            .setNegativeButton("Exit application", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // without permission exit is the only way
                                    finish();
                                }
                            })
                            .show();
                }
                break;
        }
    }

    private void initEmpaticaDeviceManager() {
        Log.d(TAG, "initEmpaticaDeviceManager Method");

        // Android 6 (API level 23) now require ACCESS_COARSE_LOCATION permission to use BLE
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_ACCESS_COARSE_LOCATION);
        } else {
            // Create a new EmpaDeviceManager. MainActivity is both its data and status delegate.
            deviceManager = new EmpaDeviceManager(getApplicationContext(), this, this);

            if (TextUtils.isEmpty(EMPATICA_API_KEY)) {
                new AlertDialog.Builder(this)
                        .setTitle("Warning")
                        .setMessage("Please insert your API KEY")
                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // without permission exit is the only way
                                finish();
                            }
                        })
                        .show();
                return;
            }
            // Initialize the Device Manager using your API key. You need to have Internet access at this point.
            Log.d(TAG, "authenticateWithAPIKey");
            deviceManager.authenticateWithAPIKey(EMPATICA_API_KEY);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (deviceManager != null) {
            deviceManager.stopScanning();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (deviceManager != null) {
            deviceManager.cleanUp();
        }
    }

    @Override
    public void didDiscoverDevice(BluetoothDevice bluetoothDevice, String deviceName, int rssi, boolean allowed) {
        // Check if the discovered device can be used with your API key. If allowed is always false,
        // the device is not linked with your API key. Please check your developer area at
        // https://www.empatica.com/connect/developer.php
        if (allowed) {
            // Stop scanning. The first allowed device will do.
            deviceManager.stopScanning();
            try {
                // Connect to the device
                deviceManager.connectDevice(bluetoothDevice);
                updateLabel(deviceNameLabel, "To: " + deviceName);
            } catch (ConnectionNotAllowedException e) {
                // This should happen only if you try to connect when allowed == false.
                Toast.makeText(MainActivity.this, "Sorry, you can't connect to this device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void didRequestEnableBluetooth() {
        // Request the user to enable Bluetooth
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // The user chose not to enable Bluetooth
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            // You should deal with this
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void didUpdateSensorStatus(EmpaSensorStatus status, EmpaSensorType type) {
        // No need to implement this right now
    }

    @Override
    public void didUpdateStatus(EmpaStatus status) {
        // Update the UI

        updateLabel(statusLabel, status.name());
        Log.d(TAG, "didUpdateStatus");

        // The device manager is ready for use
        if (status == EmpaStatus.READY) {
            Log.d(TAG, "Ready");
            updateLabel(statusLabel, status.name() + " - Turn on your device");
            // Start scanning
            deviceManager.startScanning();
            // The device manager has established a connection
        } else if (status == EmpaStatus.CONNECTED) {
            Log.d(TAG, "CONNECTED");
            // Stop streaming after STREAMING_TIME
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Disconnect device
                            deviceManager.disconnect();
                        }
                    }, STREAMING_TIME);
                }
            });
            // The device manager disconnected from a device
        } else if (status == EmpaStatus.DISCONNECTED) {
            Log.d(TAG, "DISCONNECTED");
            updateLabel(deviceNameLabel, "");
        }
    }

    @Override
    public void didReceiveAcceleration(int x, int y, int z, double timestamp) {

    }

    @Override
    public void didReceiveBVP(final float bvp, double timestamp) {
        if (firstTimeStampRead) {
            initialTime = (long) timestamp;
            firstTimeStampRead = false;
        }

        dInitialTime = initialTime;
        fCurrTimeBvp = (float) (timestamp - dInitialTime);
        dBVP = bvp;
        bvpSample.add(dBVP);
        bvpampleTimeStamps.add(fCurrTimeBvp);
        if (bvpSample.size() > 1024) {
            bvpSample.remove(0);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bvpSeries.appendData(new DataPoint(fCurrTimeBvp, dBVP), true, 1024);
                bvpSeries.setDrawBackground(true);
                bvpSeries.setAnimated(true);
                bvpSeries.setColor(Color.WHITE);
            }
        });

    }

    @Override
    public void didReceiveBatteryLevel(float battery, double timestamp) {

    }

    @Override
    public void didReceiveGSR(float gsr, double timestamp) {
        edaSample.add(gsr);
        edaSum = 0;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (edaSample.size() > 32) {
                    edaSample.remove(0);
                }

                for (int i = 0; i < edaSample.size(); i++) {
                    edaSum += edaSample.get(i);
                }
                edaAverage = edaSum / edaSample.size();
                edaAverage = Math.round(edaAverage * 1000);
                edaAverage = edaAverage / 1000;

                updateLabel(edaRateLabel, "" + edaAverage);
            }
        });
    }

    @Override
    public void didReceiveIBI(float ibi, double timestamp) {
        ibiSum = 0;
        ibiStdSum = 0;
        nn50Count = 0;
        dIBI = Math.round(ibi * 100000);
        dIBI = dIBI / 100;
        lCurTimeIBI = ((long) timestamp) - initialTime;
        fCurTimeIBI = lCurTimeIBI;

        new Thread(new Runnable() {
            public void run() {
                ibiArraySize = ibiSample.size();
                ibiSample.add(dIBI);
                ibiSampleTimeStamps.add(fCurTimeIBI);

                if (!(ibiArraySize < 10)) {
                    ibiSample.remove(0);
                    ibiSampleTimeStamps.remove(0);
                }

                for (int i = 0; i < ibiArraySize; i++) {
                    ibiSum += ibiSample.get(i);
                }

                ibiAverage = Math.round(ibiSum / (ibiArraySize + 1) * 100);
                ibiAverage = ibiAverage / 100;

                for (int i = 0; i < ibiArraySize - 1; i++) {
                    ibiStdSum += Math.pow((ibiSample.get(i) - ibiAverage), 2);
                    if (i < ibiArraySize) {
                        if (Math.abs(ibiSample.get(i) - ibiSample.get(i + 1)) > 50) {
                            nn50Count++;
                        }
                    }
                }
                ibiStd = Math.round((Math.sqrt(ibiStdSum / (ibiArraySize))) * 100000);
                ibiStd = ibiStd / 100000;
                pnn50 = (nn50Count / (ibiArraySize)) * 100;
                heartRate = Math.round(60000 / ibiAverage);
                if (ibiSample.size() > 5) {
                    updateLabel(heartRateLabel, "" + ((int) heartRate));
                }

                /*
                Log.d(TAG, "Time since readings started in seconds: " + lCurTimeIBI);
                Log.d(TAG, "IBI Sample Size: " + ibiArraySize);
                Log.d(TAG, "IBI Sum: " + ibiSum);
                Log.d(TAG, "pNN50: " + pnn50 +"%");
                Log.d(TAG, "IBI std Sum: " + ibiStdSum);
                Log.d(TAG, "IBI std: " + ibiStd);
                Log.d(TAG, "IBI mean: " + ibiAverage);
                Log.d(TAG, "IBI Samples: " + ibiSample);
                Log.d(TAG, "IBI Times: " + ibiSampleTimeStamps);
                Log.d(TAG, "Heart Rate: " + heartRate);
                */

            }
        }).start();


    }

    @Override
    public void didReceiveTemperature(float temp, double timestamp) {
        tempSample.add(temp);
        Log.d(TAG, "Temp: " + temp);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tempSum = 0;
                Log.d(TAG, "Temp sum: " + tempSum);
                if (tempSample.size() > 8) {
                    tempSample.remove(0);
                }
                for (int i = 0; i < tempSample.size(); i++) {
                    tempSum += tempSample.get(i);
                    Log.d(TAG, "Temp sum: " + tempSum + " Iteration: " + i + "curTemp: " + tempSample.get(i));
                }
                tempAverage = tempSum / tempSample.size();
                tempAverage = Math.round(tempAverage * 10);
                tempAverage = tempAverage / 10;
                updateLabel(tempRateLabel, "" + tempAverage);

                Log.d(TAG, "Temp sum: " + tempSum);
                Log.d(TAG, "Temp avg: " + tempAverage);
                Log.d(TAG, "Temp sample size: " + tempSample.size());
                Log.d(TAG, "Temp sample: " + tempSample);

            }
        });
    }

    // Update a label with some text, making sure this is run in the UI thread
    private void updateLabel(final TextView label, final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(text);
            }
        });
    }
}
